import {Box} from "@chakra-ui/core";
import React from "react";
import {Image as ImageType} from "./ImagesContainer";

interface Props {
    image: ImageType
}

const ImageBox: React.FC<Props> = ({image}) =>
    <Box minW={'25%'} width={{base: 1, sm: 1 / 2, md: 1 / 4}}>
        <img alt={`${image.id}`} src={image.url}/>
    </Box>;

export default ImageBox
