import {Box} from "@chakra-ui/core";
import React, {useContext, useEffect, useState} from "react";
import {SearchContextImpl} from "../contexts/Search";
import ImageBox from "./ImageBox";

export type Image = {
    id: number,
    url: string,
    source: "giphy" | "pixabay"
}


export const ImagesContainer: React.FC = () => {
    const {searchWord} = useContext(SearchContextImpl);
    const [images, setImages] = useState<Array<Image>>([]);

    useEffect(() => {
        if (searchWord.length < 3) {
            return setImages([])
        }
        (async () => {
            const response = await fetch(`/api/?word=${decodeURI(searchWord)}`);
            if (response.ok !== true) {
                throw Error("something went wrong")
            }
            const json = await response.json();
            setImages(json)
        })()

    }, [searchWord]);
    return <Box display={"flex"} flexWrap="wrap" flexWirection="column" flexJustify="space-evenly">{
        images
            .map(
                (image) => <ImageBox key={image.id} image={image}/>)}
    </Box>
};
