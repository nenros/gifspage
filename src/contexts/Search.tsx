import React, {useState} from "react";

interface SearchContext {
    searchWord: string,
    setSearchWord: (searchWord: string) => void
}

export const SearchContextImpl = React.createContext<SearchContext>({
    searchWord: "",
    setSearchWord: (searchWord: string) => searchWord
});

export const SearchContextProvider: React.FC = ({children}) => {
    const [searchWord, setSearchWord] = useState<string>("");
    return <SearchContextImpl.Provider value={{searchWord, setSearchWord}}>
        {children}
    </SearchContextImpl.Provider>
};
