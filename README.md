# How to run
1. Run server with command: `PIXBAY_KEY=<pixbay key> GIPHY_KEY=<giphy_key> yarn server`
1. Run frontend part using `yarn start`
1. Open [http://localhost:8080](http://localhost:8080)


# What can be done better
* I added jest but haven't written any test...
* Chakra UI was mistake with typescript
* My Webpack config could be better
* Better error handling on frontend and backend
* Maybe build can be built and served using `serve`, not by dev server
