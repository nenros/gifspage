import {Box, CSSReset, ThemeProvider} from "@chakra-ui/core";
import React from "react";
import {ImagesContainer} from "./components/ImagesContainer";
import SearchBar from "./components/SearchBar";
import {SearchContextProvider} from "./contexts/Search";

const App: React.FC = () => <ThemeProvider>
    <CSSReset/>
    <SearchContextProvider>
        <Box display="grid" maxW="960px" mx="auto">
            <SearchBar/>
            <ImagesContainer/>
        </Box>
    </SearchContextProvider>
</ThemeProvider>;

export default App
