const express = require('express');
const app = express();
const port = 3000;
const fetch = require('node-fetch');

app.get("/", async (req, res) => {
  console.log("Request in!");
  if (req.hasOwnProperty('query') && req.query.hasOwnProperty('word')) {
    const images = await findImagesByWord(req.query.word);

    return res.json(images)
  }

  res.status(400).send("You need to pass `word` query param")
});

app.listen(port, () => console.log(`Server running on port ${port}!`));


const findImagesByWord = (word) => {
  console.log(`Searching images for word: ${word}`);
  return Promise.all([
    findImagesInPixabay(word),
    findImagesInGiphy(word)
  ]).then((data) => {
    return [...data[0], ...data[1]]
  })
};


const findImagesInPixabay = async (word) => {
  const response = await fetch(`https://pixabay.com/api/?key=${process.env.PIXBAY_KEY}&q=${decodeURI(word)}&image_type=photo`);
  if (response.ok === false) {
    console.error(response);
    return []
  }
  const json = await response.json();
  return json.hits.reduce((acc, photo) =>
      acc.concat({id: photo.id, url: photo.previewURL, source: 'pixabay'})
    , [])
};

const findImagesInGiphy = async (word) => {
  const response = await fetch(`https://api.giphy.com/v1/gifs/search?key=${process.env.GIPHY_KEY}&q=${decodeURI(word)}`);
  if (response.ok === false) {
    console.error(response);
    return []
  }
  const json = await response.json();
  return json.data.reduce((acc, photo) =>
      acc.concat({id: photo.id, url: photo.images.preview_gif.url, source: 'giphy'})
    , [])
};
