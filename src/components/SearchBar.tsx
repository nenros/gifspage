import {Box} from "@chakra-ui/core";
import React, {ChangeEvent, useContext} from "react";
import {SearchContextImpl} from "../contexts/Search";

const SearchBar: React.FC = () => {
    const {setSearchWord, searchWord} = useContext(SearchContextImpl);
    const onChange = (event: ChangeEvent<HTMLInputElement>) => {
        const {value} = event.currentTarget;
        setSearchWord(value)
    };

    return <Box w="100%" >
        <input value={searchWord} placeholder={"Type word to search"} onChange={onChange}
               style={{border: "black 2px solid"}}/>
    </Box>
};

export default SearchBar
